let SYSTEM_INFO = uni.getSystemInfoSync() // 获取系统信息
let getStatusBarHeight = () => SYSTEM_INFO.statusBarHeight || 15 // 获取系统状态栏高度，没有就是15

export {
	getStatusBarHeight
}