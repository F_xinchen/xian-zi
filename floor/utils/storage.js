const setStorage = (key, value) => {
	try {
		uni.setStorageSync(key, value)
	} catch (e) {
		//TODO handle the exception
		console.error(`本地储存 ${key},发生了异常 =>`, e)
	}
}

const getStorage = (key) => {
	try {
		return uni.getStorageSync(key)
	} catch (e) {
		//TODO handle the exception
		console.error(`本地获取 ${key},发生了异常 =>`, e)
	}
}

const removeStorage = (key) => {
	try {
		uni.removeStorageSync(key)
	} catch (e) {
		//TODO handle the exception
		console.error(`本地移除 ${key},发生了异常 =>`, e)
	}
}

const clearStorage = (key) => {
	try {
		uni.clearStorageSync()
	} catch (e) {
		//TODO handle the exception
		console.error(`本地清除,发生了异常 =>`, e)
	}
}

/* ********************************************************************************************************** */

const asyncSetStorage = (key, data) => {
	return new Promise((resolve) => {

		uni.setStorage({
			key,
			data,
			complete(res) {
				resolve(res)
			}
		})

	})
}
const asyncGetStorage = (key) => {
	return new Promise((resolve) => {

		uni.getStorage({
			key,
			complete(res) {
				resolve(res)
			}
		})

	})
}

const asyncRemoveStorage = (key) => {
	return new Promise((resolve) => {

		uni.removeStorage({
			key,
			complete(res) {
				resolve(res)
			}
		})

	})
}

const asyncClearStorage = (key) => {
	return new Promise((resolve) => {

		uni.clearStorage({
			key,
			complete(res) {
				resolve(res)
			}
		})

	})
}



export {
	setStorage,
	getStorage,
	removeStorage,
	clearStorage,
	asyncSetStorage,
	asyncGetStorage,
	asyncRemoveStorage,
	asyncClearStorage
}