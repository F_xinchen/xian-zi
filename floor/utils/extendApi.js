// let { name='tom'} = {} ，这样解构赋值，得到 name = tom
// 传入参数会把默认参数进行覆盖
const toast = ({ title = '加载中...', icon = 'none', duration = 2000, mask = true } = {}) => {
	uni.showToast({
		title,
		icon,
		duration,
		mask
	})
}

// 在方法内部需要通过 Promise 返回用户的操作
const modal = (option = {}) => {

	return new Promise((resolve) => {
		// 默认的参数
		const defaultOpt = {
			title: '提示',
			content: '您确定执行该操作吗？',
			confirmColor: '#f3514f'
		}

		// 通过 Object.assign 方法将参数进行合并
		// 需要使用传入的参数覆盖默认的参数
		// 为了不影响默认的参数，需要将合并以后的参数赋值给一个空对象
		let opts = Object.assign({}, defaultOpt, option)

		uni.showModal({
			...opts,
			success: ({ cancel, confirm }) => {
				if (confirm) {
					resolve(true)
				} else {
					resolve(false)
				}
			}
		})
	})

}


export {
	toast,
	modal
}