let date = new Date();

const formatTime = () => {
	const year = date.getFullYear();
	const month = date.getMonth() + 1;
	const day = date.getDate();
	const hours = date.getHours();
	const minutes = date.getMinutes();
	const seconds = date.getSeconds();

	return `${[year,month,day].map(formatNumber).join("/") } ${[hours,minutes,seconds].map(formatNumber).join(':')}`
}

const formatNumber = number => {
	let n = number.toString()

	return `${n[1] ? n : '0'+n}`
}

export default formatTime