import http from '@/utils/http.js'

// 商品详情加入购物车 及 购物车更新商品数量
export const reqAddCart = ({ goodsId, count, ...data }) => {
	return http.get(`/cart/addToCart/${goodsId}/${count}`, data)
}

// 获取购物车列表数据
export const reqCartList = () => {
	return http.get('/cart/getCartList')
}

// 更新商品选择状态
export const reqUpdateCheked = (goodsId, isChecked) => {
	return http.get(`/cart/checkCart/${goodsId}/${isChecked}`)
}

// 全选或全部选
export const reqChekeAllStatus = (isChecked) => {
	return http.get(`/cart/checkAllCart/${isChecked}`)
}

// 删除购物车商品
export const reqDelCartGoods = (goodsId) => {
	return http.get(`/cart/delete/${goodsId}`)
}