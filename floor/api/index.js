import http from '@/utils/http.js'

// 通过并发请求，提高渲染速度
export const reqIndexData = () => {
	return http.all(
		http.get('/index/findBanner'),
		http.get('/index/findCategory1'),
		http.get('/index/advertisement'),
		http.get('/index/findListGoods'),
		http.get('/index/findRecommendGoods')
	)
}