import http from '@/utils/http.js'

export const reqCategoryData = () => {
	return http.get('/index/findCategoryTree')
}