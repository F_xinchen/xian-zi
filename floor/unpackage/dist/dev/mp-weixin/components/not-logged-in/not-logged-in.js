"use strict";
const common_vendor = require("../../common/vendor.js");
const store_modules_user = require("../../store/modules/user.js");
require("../../utils/storage.js");
const _sfc_main = {
  __name: "not-logged-in",
  props: {
    obj: {
      type: Object,
      default() {
        return {
          text: "您尚未登录，点击登录获取更多权益",
          button: "去登陆",
          url: "/pages/login/login",
          toTabbar: false
        };
      }
    }
  },
  setup(__props) {
    const { obj } = __props;
    store_modules_user.useUserStore();
    const toLogin = () => {
      if (obj.toTabbar) {
        common_vendor.index.switchTab({
          url: obj.url
        });
      } else {
        common_vendor.index.navigateTo({
          url: obj.url
        });
      }
    };
    return (_ctx, _cache) => {
      return {
        a: common_vendor.t(__props.obj.text),
        b: common_vendor.t(__props.obj.button),
        c: __props.obj.button,
        d: common_vendor.o(toLogin)
      };
    };
  }
};
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-3fc552c7"], ["__file", "E:/前端/微信小程序/代码/慕尚花坊/floor/components/not-logged-in/not-logged-in.vue"]]);
wx.createComponent(Component);
