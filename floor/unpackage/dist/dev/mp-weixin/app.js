"use strict";
Object.defineProperty(exports, Symbol.toStringTag, { value: "Module" });
const common_vendor = require("./common/vendor.js");
const store_index = require("./store/index.js");
if (!Math) {
  "./pages/index/index.js";
  "./pages/category/category.js";
  "./pages/cart/cart.js";
  "./pages/my/my.js";
  "./pages/login/login.js";
  "./pages/settings/settings.js";
  "./components/good-list/good-list.js";
  "./components/good-card/good-card.js";
  "./pages/index/skeleton/index_skeleton.js";
  "./modules/settingModule/pages/address/list/list.js";
  "./modules/settingModule/pages/address/add/add.js";
  "./modules/settingModule/pages/profile/profile.js";
  "./modules/goodModule/pages/goods/list/list.js";
  "./modules/goodModule/pages/goods/detail/detail.js";
  "./modules/orderPayModule/pages/order/list/list.js";
  "./modules/orderPayModule/pages/order/detail/detail.js";
}
const _sfc_main = {
  onLaunch: function() {
    console.log("App Launch");
  },
  onShow: function() {
    console.log("App Show");
  },
  onHide: function() {
    console.log("App Hide");
  },
  globalData: {
    address: {}
  }
};
const App = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__file", "E:/前端/微信小程序/代码/慕尚花坊/floor/App.vue"]]);
function createApp() {
  const app = common_vendor.createSSRApp(App);
  app.use(store_index.pinia);
  return {
    app
  };
}
createApp().app.mount("#app");
exports.createApp = createApp;
