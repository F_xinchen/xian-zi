"use strict";
const common_vendor = require("../../common/vendor.js");
const utils_extendApi = require("../../utils/extendApi.js");
const utils_storage = require("../../utils/storage.js");
const api_user = require("../../api/user.js");
const store_modules_user = require("../../store/modules/user.js");
require("../../utils/http.js");
require("../../utils/request.js");
require("../../utils/env.js");
const _sfc_main = {
  __name: "login",
  setup(__props) {
    let userStore = store_modules_user.useUserStore();
    const login = () => {
      common_vendor.index.login({
        success: async ({ code }) => {
          if (code) {
            let result = await api_user.reqLogin(code);
            utils_storage.setStorage("token", result.data.token);
            userStore.setToken(result.data.token);
            getUserInfo();
            common_vendor.index.navigateBack();
          } else {
            utils_extendApi.toast({
              title: "用户授权失败"
            });
          }
        }
      });
    };
    const getUserInfo = async () => {
      let { data } = await api_user.reqUserInfo();
      utils_storage.setStorage("userInfo", data);
      userStore.setUserInfo(data);
    };
    return (_ctx, _cache) => {
      return {
        a: common_vendor.o(login)
      };
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-e4e4508d"], ["__file", "E:/前端/微信小程序/代码/慕尚花坊/floor/pages/login/login.vue"]]);
wx.createPage(MiniProgramPage);
