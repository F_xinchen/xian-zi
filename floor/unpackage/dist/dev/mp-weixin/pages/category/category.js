"use strict";
const common_vendor = require("../../common/vendor.js");
require("../../utils/http.js");
const api_category = require("../../api/category.js");
require("../../utils/request.js");
require("../../utils/extendApi.js");
require("../../utils/storage.js");
require("../../utils/env.js");
const _sfc_main = {
  __name: "category",
  setup(__props) {
    let categoryList = common_vendor.ref([
      [
        { children: [] }
      ]
    ]);
    let activeIndex = common_vendor.ref(0);
    const updateActive = (e) => {
      activeIndex.value = e.currentTarget.dataset.index;
    };
    const getCategoryData = async () => {
      let res = await api_category.reqCategoryData();
      if (res.code == 200) {
        categoryList.value = res.data;
      }
    };
    common_vendor.onLoad(() => {
      getCategoryData();
    });
    return (_ctx, _cache) => {
      return {
        a: common_vendor.f(common_vendor.unref(categoryList), (item, index, i0) => {
          return {
            a: common_vendor.t(item.name),
            b: common_vendor.n(`left-view-item ${common_vendor.unref(activeIndex) === index ? "active" : ""}`),
            c: item.id,
            d: common_vendor.o(updateActive, item.id)
          };
        }),
        b: common_vendor.f(common_vendor.unref(categoryList)[common_vendor.unref(activeIndex)].children, (item, index, i0) => {
          return {
            a: item.imageUrl,
            b: common_vendor.t(item.name),
            c: `/modules/goodModule/pages/goods/list/list?category2Id=${item.id}`,
            d: item.id
          };
        })
      };
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-8145b772"], ["__file", "E:/前端/微信小程序/代码/慕尚花坊/floor/pages/category/category.vue"]]);
wx.createPage(MiniProgramPage);
