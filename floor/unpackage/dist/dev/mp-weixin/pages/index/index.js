"use strict";
const common_vendor = require("../../common/vendor.js");
require("../../utils/http.js");
const api_index = require("../../api/index.js");
require("../../utils/request.js");
require("../../utils/extendApi.js");
require("../../utils/storage.js");
require("../../utils/env.js");
if (!Array) {
  const _component_tepmlate = common_vendor.resolveComponent("tepmlate");
  const _easycom_uni_swiper_dot2 = common_vendor.resolveComponent("uni-swiper-dot");
  const _easycom_good_list2 = common_vendor.resolveComponent("good-list");
  (_component_tepmlate + _easycom_uni_swiper_dot2 + _easycom_good_list2)();
}
const _easycom_uni_swiper_dot = () => "../../uni_modules/uni-swiper-dot/components/uni-swiper-dot/uni-swiper-dot.js";
const _easycom_good_list = () => "../../components/good-list/good-list2.js";
if (!Math) {
  (skeleton + _easycom_uni_swiper_dot + _easycom_good_list)();
}
const skeleton = () => "./skeleton/index_skeleton2.js";
const _sfc_main = {
  __name: "index",
  setup(__props) {
    let loding = common_vendor.ref(true);
    let current = common_vendor.ref(0);
    let dotsStyles = common_vendor.ref({
      width: 10,
      backgroundColor: "rgba(239, 7, 23, 0.3)",
      border: "1px rgba(0, 0, 0, .3) solid",
      selectedBackgroundColor: "rgba(239, 7, 23, 0.8)",
      selectedBorder: "1px rgba(0, 0, 0, .9) solid"
    });
    let data = common_vendor.reactive({
      bannerList: [],
      // 轮播图
      categoryList: [],
      //商品导航
      activeList: [],
      // 活动渲染区域
      hotList: [],
      // 人气推荐
      guessList: []
      // 猜你喜欢
    });
    const getIndexData = async () => {
      let reslove = await api_index.reqIndexData();
      data.bannerList = reslove[0].data;
      data.categoryList = reslove[1].data;
      data.activeList = reslove[2].data;
      data.guessList = reslove[3].data;
      data.hotList = reslove[4].data;
      loding.value = false;
    };
    const swiperChange = (e) => {
      current.value = e.detail.current;
    };
    common_vendor.onLoad(() => {
      getIndexData();
    });
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: common_vendor.unref(loding)
      }, common_vendor.unref(loding) ? {} : {
        b: common_vendor.f(common_vendor.unref(data).bannerList, (item, k0, i0) => {
          return {
            a: item.imageUrl,
            b: item.id
          };
        }),
        c: common_vendor.o(swiperChange),
        d: common_vendor.p({
          info: common_vendor.unref(data).bannerList,
          current: common_vendor.unref(current),
          mode: "default",
          ["dots-styles"]: common_vendor.unref(dotsStyles)
        }),
        e: common_vendor.f(common_vendor.unref(data).categoryList, (item, k0, i0) => {
          return {
            a: item.imageUrl,
            b: common_vendor.t(item.name),
            c: `/modules/goodModule/pages/goods/list/list?category1Id=${item.id}`,
            d: item.id
          };
        }),
        f: common_vendor.f(common_vendor.unref(data).activeList, (item, k0, i0) => {
          return {
            a: item.imageUrl,
            b: `/modules/goodModule/pages/goods/list/list?category2Id=${item.category2Id}`,
            c: item.id
          };
        }),
        g: common_vendor.p({
          title: "猜你喜欢",
          list: common_vendor.unref(data).guessList
        }),
        h: common_vendor.p({
          title: "人气推荐",
          list: common_vendor.unref(data).hotList
        })
      });
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-1cf27b2a"], ["__file", "E:/前端/微信小程序/代码/慕尚花坊/floor/pages/index/index.vue"]]);
wx.createPage(MiniProgramPage);
