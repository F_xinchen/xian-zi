"use strict";
const common_vendor = require("../../common/vendor.js");
const store_modules_user = require("../../store/modules/user.js");
require("../../utils/storage.js");
const _sfc_main = {
  __name: "my",
  setup(__props) {
    let userStore = store_modules_user.useUserStore();
    let initpanel = common_vendor.ref([
      {
        url: "/modules/orderPayModule/pages/order/list/list",
        title: "商品订单",
        iconfont: "icon-dingdan"
      },
      {
        url: "/modules/orderPayModule/pages/order/list/list",
        title: "礼品卡订单",
        iconfont: "icon-lipinka"
      },
      {
        url: "/modules/orderPayModule/pages/order/list/list",
        title: "退款/售后",
        iconfont: "icon-tuikuan"
      }
    ]);
    const toLoginPage = () => {
      common_vendor.index.navigateTo({
        url: "/pages/login/login"
      });
    };
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: !common_vendor.unref(userStore).token
      }, !common_vendor.unref(userStore).token ? {
        b: common_vendor.o(toLoginPage)
      } : {
        c: common_vendor.unref(userStore).userInfo.headimgurl,
        d: common_vendor.t(common_vendor.unref(userStore).userInfo.nickname)
      }, {
        e: common_vendor.f(common_vendor.unref(initpanel), (item, index, i0) => {
          return {
            a: common_vendor.n(item.iconfont),
            b: common_vendor.t(item.title),
            c: `${common_vendor.unref(userStore).token ? item.url : "/pages/login/login"}`,
            d: index
          };
        })
      });
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-2f1ef635"], ["__file", "E:/前端/微信小程序/代码/慕尚花坊/floor/pages/my/my.vue"]]);
wx.createPage(MiniProgramPage);
