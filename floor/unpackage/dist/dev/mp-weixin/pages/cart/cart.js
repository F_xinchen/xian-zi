"use strict";
const common_vendor = require("../../common/vendor.js");
const store_modules_user = require("../../store/modules/user.js");
const api_cartReq = require("../../api/cartReq.js");
const utils_extendApi = require("../../utils/extendApi.js");
require("../../utils/storage.js");
require("../../utils/http.js");
require("../../utils/request.js");
require("../../utils/env.js");
if (!Array) {
  const _easycom_uni_number_box2 = common_vendor.resolveComponent("uni-number-box");
  const _easycom_uni_swipe_action_item2 = common_vendor.resolveComponent("uni-swipe-action-item");
  const _easycom_uni_swipe_action2 = common_vendor.resolveComponent("uni-swipe-action");
  const _easycom_not_logged_in2 = common_vendor.resolveComponent("not-logged-in");
  (_easycom_uni_number_box2 + _easycom_uni_swipe_action_item2 + _easycom_uni_swipe_action2 + _easycom_not_logged_in2)();
}
const _easycom_uni_number_box = () => "../../uni_modules/uni-number-box/components/uni-number-box/uni-number-box.js";
const _easycom_uni_swipe_action_item = () => "../../uni_modules/uni-swipe-action/components/uni-swipe-action-item/uni-swipe-action-item.js";
const _easycom_uni_swipe_action = () => "../../uni_modules/uni-swipe-action/components/uni-swipe-action/uni-swipe-action.js";
const _easycom_not_logged_in = () => "../../components/not-logged-in/not-logged-in.js";
if (!Math) {
  (_easycom_uni_number_box + _easycom_uni_swipe_action_item + _easycom_uni_swipe_action + _easycom_not_logged_in)();
}
const _sfc_main = {
  __name: "cart",
  setup(__props) {
    let userStore = store_modules_user.useUserStore();
    let cartList = common_vendor.ref([]);
    let options = common_vendor.ref(
      [{
        text: "删除",
        style: {
          backgroundColor: "#dd524d"
        }
      }]
    );
    let swipe = common_vendor.ref();
    const checkedAllGood = common_vendor.computed(() => {
      return cartList.value.length !== 0 && cartList.value.every((item) => item.isChecked == 1);
    });
    const selectAllStatus = async () => {
      let status = !checkedAllGood.value ? 1 : 0;
      cartList.value.forEach((item) => {
        item.isChecked = status;
      });
      await api_cartReq.reqChekeAllStatus(status);
    };
    const getCartList = async () => {
      let { code, data } = await api_cartReq.reqCartList();
      if (code == 200) {
        cartList.value = data;
      }
    };
    const changeBuyNumAntiShake = () => {
      let time = null;
      return function(newBuyNum, id, index, count) {
        clearTimeout(time);
        const disCount = newBuyNum - count;
        if (disCount == 0)
          return;
        time = setTimeout(async () => {
          let result = await api_cartReq.reqAddCart({ goodsId: id, count: disCount });
          if (result.code == 200) {
            cartList.value.forEach((item) => {
              if (item.goodsId == id) {
                item.count = newBuyNum;
                item.isChecked = 1;
                return;
              }
            });
          }
        }, 500);
      };
    };
    const changeBuyNum = changeBuyNumAntiShake();
    const checkboxChange = async (id) => {
      let currCheck = 0;
      cartList.value.forEach((item) => {
        if (item.goodsId == id) {
          item.isChecked = item.isChecked === 1 ? 0 : 1;
          currCheck = item.isChecked;
          return;
        }
      });
      await api_cartReq.reqUpdateCheked(id, currCheck);
    };
    const deleteItem = async (id) => {
      let modelRes = await common_vendor.index.showModal({
        title: "是否删除该商品"
      });
      if (modelRes.confirm) {
        let result = await api_cartReq.reqDelCartGoods(id);
        if (result.code == 200) {
          utils_extendApi.toast({ title: "删除成功" });
          getCartList();
        }
      }
    };
    const closeAllSwipe = () => {
      swipe.value.closeAll();
    };
    const totalPrice = common_vendor.computed(() => {
      let sumPrice = 0;
      cartList.value.forEach((item) => {
        if (item.isChecked == 1) {
          sumPrice += item.price * item.count;
        }
      });
      return sumPrice;
    });
    const toOrder = () => {
      if (totalPrice.value === 0) {
        utils_extendApi.toast({ title: "请选择商品" });
      } else {
        common_vendor.index.navigateTo({
          url: "/modules/orderPayModule/pages/order/detail/detail"
        });
      }
    };
    common_vendor.onHide(() => {
      swipe.value.closeAll();
    });
    common_vendor.onShow(() => {
      getCartList();
    });
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: common_vendor.unref(userStore).token && common_vendor.unref(cartList).length
      }, common_vendor.unref(userStore).token && common_vendor.unref(cartList).length ? {
        b: common_vendor.f(common_vendor.unref(cartList), (item, index, i0) => {
          return {
            a: common_vendor.o(($event) => checkboxChange(item.goodsId), item.goodsId),
            b: item.isChecked,
            c: `${item.imageUrl}`,
            d: common_vendor.t(item.name),
            e: common_vendor.t(item.price),
            f: common_vendor.o(($event) => common_vendor.unref(changeBuyNum)($event, item.goodsId, index, item.count), item.goodsId),
            g: "c91e7611-2-" + i0 + "," + ("c91e7611-1-" + i0),
            h: common_vendor.p({
              value: `${item.count}`,
              min: 1,
              max: 200
            }),
            i: common_vendor.o(($event) => deleteItem(item.goodsId), item.goodsId),
            j: "c91e7611-1-" + i0 + ",c91e7611-0",
            k: item.goodsId
          };
        }),
        c: common_vendor.p({
          ["right-options"]: common_vendor.unref(options)
        }),
        d: common_vendor.sr(swipe, "c91e7611-0", {
          "k": "swipe"
        }),
        e: common_vendor.o(closeAllSwipe)
      } : common_vendor.e({
        f: common_vendor.unref(userStore).token && common_vendor.unref(cartList) == 0
      }, common_vendor.unref(userStore).token && common_vendor.unref(cartList) == 0 ? {
        g: common_vendor.p({
          obj: {
            text: "还没有添加商品，去添加~~~",
            button: "去购物",
            url: "/pages/index/index",
            toTabbar: true
          }
        })
      } : {}), {
        h: common_vendor.unref(cartList).length && common_vendor.unref(userStore).token
      }, common_vendor.unref(cartList).length && common_vendor.unref(userStore).token ? {
        i: common_vendor.o(selectAllStatus),
        j: common_vendor.unref(checkedAllGood),
        k: common_vendor.t(common_vendor.unref(totalPrice)),
        l: common_vendor.o(toOrder),
        m: common_vendor.o(closeAllSwipe)
      } : {});
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-c91e7611"], ["__file", "E:/前端/微信小程序/代码/慕尚花坊/floor/pages/cart/cart.vue"]]);
wx.createPage(MiniProgramPage);
