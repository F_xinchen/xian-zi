"use strict";
const utils_http = require("../utils/http.js");
const reqIndexData = () => {
  return utils_http.instance.all(
    utils_http.instance.get("/index/findBanner"),
    utils_http.instance.get("/index/findCategory1"),
    utils_http.instance.get("/index/advertisement"),
    utils_http.instance.get("/index/findListGoods"),
    utils_http.instance.get("/index/findRecommendGoods")
  );
};
exports.reqIndexData = reqIndexData;
