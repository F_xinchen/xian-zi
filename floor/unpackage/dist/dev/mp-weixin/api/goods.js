"use strict";
const utils_http = require("../utils/http.js");
const reqGoodsList = ({ page, limit, ...data }) => {
  return utils_http.instance.get(`/goods/list/${page}/${limit}`, data);
};
const reqGoodsInfo = (goodsId) => {
  return utils_http.instance.get(`/goods/${goodsId}`);
};
exports.reqGoodsInfo = reqGoodsInfo;
exports.reqGoodsList = reqGoodsList;
