"use strict";
const utils_http = require("../utils/http.js");
const reqOrderInfo = () => {
  return utils_http.instance.get("/order/trade");
};
const reqOrderAdress = () => {
  return utils_http.instance.get("/userAddress/getOrderAddress");
};
const reqBuyNowGoods = ({ goodsId, ...data }) => {
  return utils_http.instance.get(`/order/buy/${goodsId}`, data);
};
const reqSubmitOrder = (data) => {
  return utils_http.instance.post(`/order/submitOrder`, data);
};
const reqPrePayInfo = (orderNo) => {
  return utils_http.instance.get(`/webChat/createJsapi/${orderNo}`);
};
const reqPayStatus = (orderNo) => {
  return utils_http.instance.get(`/webChat/queryPayStatus/${orderNo}`);
};
const reqOrderList = (page, limit) => {
  return utils_http.instance.get(`/order/order/${page}/${limit}`);
};
exports.reqBuyNowGoods = reqBuyNowGoods;
exports.reqOrderAdress = reqOrderAdress;
exports.reqOrderInfo = reqOrderInfo;
exports.reqOrderList = reqOrderList;
exports.reqPayStatus = reqPayStatus;
exports.reqPrePayInfo = reqPrePayInfo;
exports.reqSubmitOrder = reqSubmitOrder;
