"use strict";
const utils_http = require("../utils/http.js");
const reqAddAddress = (data) => {
  return utils_http.instance.post("/userAddress/save", data);
};
const reqAddressList = () => {
  return utils_http.instance.get("/userAddress/findUserAddress");
};
const reqAddressInfo = (id) => {
  return utils_http.instance.get("/userAddress/" + id);
};
const reqUpdateAddress = (data) => {
  return utils_http.instance.post("/userAddress/update", data);
};
const reqDelAddress = (id) => {
  return utils_http.instance.get("/userAddress/delete/" + id);
};
exports.reqAddAddress = reqAddAddress;
exports.reqAddressInfo = reqAddressInfo;
exports.reqAddressList = reqAddressList;
exports.reqDelAddress = reqDelAddress;
exports.reqUpdateAddress = reqUpdateAddress;
