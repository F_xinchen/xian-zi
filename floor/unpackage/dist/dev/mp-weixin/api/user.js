"use strict";
const utils_http = require("../utils/http.js");
const reqLogin = (code) => {
  return utils_http.instance.get(`/weixin/wxLogin/${code}`);
};
const reqUserInfo = () => {
  return utils_http.instance.get("/weixin/getuserInfo");
};
const reqUpdateUserInfo = (userInfo) => {
  return utils_http.instance.post("/weixin/updateUser", userInfo);
};
exports.reqLogin = reqLogin;
exports.reqUpdateUserInfo = reqUpdateUserInfo;
exports.reqUserInfo = reqUserInfo;
