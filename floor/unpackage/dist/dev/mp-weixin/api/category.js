"use strict";
const utils_http = require("../utils/http.js");
const reqCategoryData = () => {
  return utils_http.instance.get("/index/findCategoryTree");
};
exports.reqCategoryData = reqCategoryData;
