"use strict";
const utils_http = require("../utils/http.js");
const reqAddCart = ({ goodsId, count, ...data }) => {
  return utils_http.instance.get(`/cart/addToCart/${goodsId}/${count}`, data);
};
const reqCartList = () => {
  return utils_http.instance.get("/cart/getCartList");
};
const reqUpdateCheked = (goodsId, isChecked) => {
  return utils_http.instance.get(`/cart/checkCart/${goodsId}/${isChecked}`);
};
const reqChekeAllStatus = (isChecked) => {
  return utils_http.instance.get(`/cart/checkAllCart/${isChecked}`);
};
const reqDelCartGoods = (goodsId) => {
  return utils_http.instance.get(`/cart/delete/${goodsId}`);
};
exports.reqAddCart = reqAddCart;
exports.reqCartList = reqCartList;
exports.reqChekeAllStatus = reqChekeAllStatus;
exports.reqDelCartGoods = reqDelCartGoods;
exports.reqUpdateCheked = reqUpdateCheked;
