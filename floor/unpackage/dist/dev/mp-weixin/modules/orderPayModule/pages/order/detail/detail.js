"use strict";
const common_vendor = require("../../../../../common/vendor.js");
const utils_extendApi = require("../../../../../utils/extendApi.js");
const api_orderpay = require("../../../../../api/orderpay.js");
require("../../../../../utils/http.js");
require("../../../../../utils/request.js");
require("../../../../../utils/storage.js");
require("../../../../../utils/env.js");
if (!Array) {
  const _easycom_uni_icons2 = common_vendor.resolveComponent("uni-icons");
  const _easycom_uni_easyinput2 = common_vendor.resolveComponent("uni-easyinput");
  const _easycom_uni_forms_item2 = common_vendor.resolveComponent("uni-forms-item");
  const _easycom_uni_datetime_picker2 = common_vendor.resolveComponent("uni-datetime-picker");
  const _easycom_uni_forms2 = common_vendor.resolveComponent("uni-forms");
  (_easycom_uni_icons2 + _easycom_uni_easyinput2 + _easycom_uni_forms_item2 + _easycom_uni_datetime_picker2 + _easycom_uni_forms2)();
}
const _easycom_uni_icons = () => "../../../../../uni_modules/uni-icons/components/uni-icons/uni-icons.js";
const _easycom_uni_easyinput = () => "../../../../../uni_modules/uni-easyinput/components/uni-easyinput/uni-easyinput.js";
const _easycom_uni_forms_item = () => "../../../../../uni_modules/uni-forms/components/uni-forms-item/uni-forms-item.js";
const _easycom_uni_datetime_picker = () => "../../../../../uni_modules/uni-datetime-picker/components/uni-datetime-picker/uni-datetime-picker.js";
const _easycom_uni_forms = () => "../../../../../uni_modules/uni-forms/components/uni-forms/uni-forms.js";
if (!Math) {
  (_easycom_uni_icons + _easycom_uni_easyinput + _easycom_uni_forms_item + _easycom_uni_datetime_picker + _easycom_uni_forms)();
}
const _sfc_main = {
  __name: "detail",
  setup(__props) {
    let form = common_vendor.ref();
    let formData = common_vendor.reactive({
      name: "",
      //订购人姓名
      phone: "",
      date: Date.now(),
      remarks: ""
    });
    let orderAddress = common_vendor.reactive({});
    let orderInfo = common_vendor.reactive({});
    let detailToNowBuy = common_vendor.reactive({});
    let orderNo = common_vendor.ref("");
    const valiName = (rule, value, data, callback) => {
      let regexp = /^[\u4e00-\u9fa5]{2,6}$/;
      if (regexp.test(value)) {
        callback();
      } else {
        callback("请输入2-6个中文字符");
      }
    };
    const valiPhone = (rule, value, data, callback) => {
      let regexp = /^1[3-9]\d{9}$/;
      if (regexp.test(value)) {
        callback();
      } else {
        callback("请输入大陆手机号格式");
      }
    };
    let rules = common_vendor.ref({
      name: {
        rules: [
          {
            required: true,
            validateFunction: valiName,
            errorMessage: "请填写{label}"
          }
        ],
        label: "姓名"
      },
      phone: {
        rules: [
          {
            required: true,
            validateFunction: valiPhone,
            errorMessage: "请填写手机号"
          }
        ]
      },
      date: { rules: [{ required: true, errorMessage: "请选择日期" }] },
      remarks: { rules: [{ required: true, errorMessage: "请填写备注信息" }] }
    });
    const submit = () => {
      const params = {
        buyName: formData.name,
        buyPhone: formData.phone,
        cartList: orderInfo.cartVoList,
        deliveryDate: formData.date,
        remark: formData.remarks,
        userAddressId: orderAddress.id
      };
      form.value.validate().then(async () => {
        let res = await api_orderpay.reqSubmitOrder(params);
        orderNo.value = res.data;
        advancePay();
      }).catch((err) => {
        utils_extendApi.toast({ title: "提交失败" });
      });
    };
    const advancePay = async () => {
      let payParams = await api_orderpay.reqPrePayInfo(orderNo.value);
      let servicePay = await common_vendor.index.getProvider({
        service: "payment"
      });
      let payInfo = await common_vendor.index.requestPayment({
        provider: servicePay.provider[0],
        ...payParams.data
      });
      if (payInfo.errMsg == "requestPayment:ok") {
        let payStatus = await api_orderpay.reqPayStatus(orderNo.value);
        if (payStatus.code == 200) {
          common_vendor.index.navigateTo({
            url: "/modules/orderPayModule/pages/order/list/list",
            success: () => {
              utils_extendApi.toast({ title: "支付成功", icon: "success" });
            }
          });
        }
      }
    };
    const getAddress = async () => {
      if (getApp().globalData.address.id) {
        Object.assign(orderAddress, getApp().globalData.address);
        return;
      }
      let result = await api_orderpay.reqOrderAdress();
      if (!result.data) {
        Object.assign(orderAddress, { id: null });
        return;
      }
      Object.assign(orderAddress, result.data);
    };
    const getOrderInfo = async () => {
      let { goodsId, blessing } = detailToNowBuy;
      let result = goodsId ? await api_orderpay.reqBuyNowGoods({ goodsId, blessing }) : await api_orderpay.reqOrderInfo();
      Object.assign(orderInfo, result.data);
      let orderGoods = orderInfo.cartVoList.find((item) => item.blessing !== "");
      formData.remarks = orderGoods ? orderGoods.blessing : "";
    };
    common_vendor.onLoad((options) => {
      Object.assign(detailToNowBuy, options);
    });
    common_vendor.onShow(() => {
      getAddress();
      getOrderInfo();
    });
    common_vendor.onReady(() => {
      form.value.setRules(rules.value);
    });
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: !common_vendor.unref(orderAddress).id
      }, !common_vendor.unref(orderAddress).id ? {
        b: common_vendor.p({
          type: "plus-filled",
          size: "20"
        })
      } : {
        c: common_vendor.t(common_vendor.unref(orderAddress).fullAddress),
        d: common_vendor.t(common_vendor.unref(orderAddress).name),
        e: common_vendor.t(common_vendor.unref(orderAddress).phone),
        f: common_vendor.p({
          color: "#bbb",
          type: "right",
          size: "23"
        })
      }, {
        g: common_vendor.o(($event) => common_vendor.unref(formData).name = $event),
        h: common_vendor.p({
          clearable: false,
          type: "text",
          placeholder: "请输入姓名",
          modelValue: common_vendor.unref(formData).name
        }),
        i: common_vendor.p({
          label: "姓名",
          name: "name"
        }),
        j: common_vendor.o(($event) => common_vendor.unref(formData).phone = $event),
        k: common_vendor.p({
          clearable: false,
          type: "number",
          placeholder: "请输入手机号",
          modelValue: common_vendor.unref(formData).phone
        }),
        l: common_vendor.p({
          label: "手机号",
          name: "phone"
        }),
        m: common_vendor.o(($event) => common_vendor.unref(formData).date = $event),
        n: common_vendor.p({
          type: "date",
          start: Date.now(),
          end: Date.now() + 3 * 30 * 24 * 60 * 60 * 1e3,
          modelValue: common_vendor.unref(formData).date
        }),
        o: common_vendor.p({
          label: "送达日期(三个月内)",
          name: "date"
        }),
        p: common_vendor.o(($event) => common_vendor.unref(formData).remarks = $event),
        q: common_vendor.p({
          maxlength: -1,
          clearable: false,
          type: "textarea",
          autoHeight: true,
          placeholder: "备注内容",
          modelValue: common_vendor.unref(formData).remarks
        }),
        r: common_vendor.p({
          label: "订单备注",
          name: "remarks"
        }),
        s: common_vendor.sr(form, "3c58b4bd-2", {
          "k": "form"
        }),
        t: common_vendor.p({
          rules: common_vendor.unref(rules),
          modelValue: common_vendor.unref(formData)
        }),
        v: common_vendor.f(common_vendor.unref(orderInfo).cartVoList, (item, index, i0) => {
          return {
            a: item.imageUrl,
            b: common_vendor.t(item.name),
            c: common_vendor.t(item.price),
            d: common_vendor.t(item.count),
            e: item.goodsId
          };
        }),
        w: common_vendor.t(common_vendor.unref(orderInfo).totalAmount),
        x: common_vendor.o(submit)
      });
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-3c58b4bd"], ["__file", "E:/前端/微信小程序/代码/慕尚花坊/floor/modules/orderPayModule/pages/order/detail/detail.vue"]]);
wx.createPage(MiniProgramPage);
