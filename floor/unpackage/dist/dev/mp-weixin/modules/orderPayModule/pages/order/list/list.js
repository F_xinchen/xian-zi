"use strict";
const common_vendor = require("../../../../../common/vendor.js");
const api_orderpay = require("../../../../../api/orderpay.js");
require("../../../../../utils/http.js");
require("../../../../../utils/request.js");
require("../../../../../utils/extendApi.js");
require("../../../../../utils/storage.js");
require("../../../../../utils/env.js");
if (!Array) {
  const _easycom_not_logged_in2 = common_vendor.resolveComponent("not-logged-in");
  _easycom_not_logged_in2();
}
const _easycom_not_logged_in = () => "../../../../../components/not-logged-in/not-logged-in.js";
if (!Math) {
  _easycom_not_logged_in();
}
const _sfc_main = {
  __name: "list",
  setup(__props) {
    let orderList = common_vendor.ref([]);
    let page = common_vendor.ref(1);
    let limit = common_vendor.ref(10);
    common_vendor.ref(0);
    const getOrderList = async () => {
      let res = await api_orderpay.reqOrderList(page.value, limit.value);
      if (res.code == 200) {
        orderList.value = [...orderList.value, ...res.data.records];
      }
    };
    common_vendor.onShow(() => {
      getOrderList();
    });
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: common_vendor.unref(orderList).length
      }, common_vendor.unref(orderList).length ? {
        b: common_vendor.f(common_vendor.unref(orderList), (item, index, i0) => {
          return {
            a: common_vendor.t(item.orderNo),
            b: common_vendor.t(`${item.orderStatus === 1 ? "已支付" : "未支付"}`),
            c: common_vendor.n(`order-status ${item.orderStatus === 1 ? "order-active" : ""}`),
            d: common_vendor.f(item.orderDetailList, (item2, index2, i1) => {
              return {
                a: `${item2.imageUrl}`,
                b: common_vendor.t(item2.name),
                c: common_vendor.t(item2.price),
                d: common_vendor.t(item2.count)
              };
            }),
            e: common_vendor.t(item.totalAmount),
            f: item.id
          };
        }),
        c: _ctx.id
      } : {
        d: common_vendor.p({
          obj: {
            text: " 还没有购买商品，快去购买吧～",
            button: "去购物",
            url: "/pages/index/index",
            toTabbar: true
          }
        })
      });
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-ad1ac141"], ["__file", "E:/前端/微信小程序/代码/慕尚花坊/floor/modules/orderPayModule/pages/order/list/list.vue"]]);
wx.createPage(MiniProgramPage);
