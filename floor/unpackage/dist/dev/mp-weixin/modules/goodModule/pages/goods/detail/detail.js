"use strict";
const common_vendor = require("../../../../../common/vendor.js");
const api_goods = require("../../../../../api/goods.js");
const api_cartReq = require("../../../../../api/cartReq.js");
const utils_extendApi = require("../../../../../utils/extendApi.js");
const store_modules_user = require("../../../../../store/modules/user.js");
require("../../../../../utils/http.js");
require("../../../../../utils/request.js");
require("../../../../../utils/storage.js");
require("../../../../../utils/env.js");
if (!Array) {
  const _easycom_uni_icons2 = common_vendor.resolveComponent("uni-icons");
  const _easycom_uni_badge2 = common_vendor.resolveComponent("uni-badge");
  const _easycom_uni_number_box2 = common_vendor.resolveComponent("uni-number-box");
  const _easycom_uni_popup2 = common_vendor.resolveComponent("uni-popup");
  (_easycom_uni_icons2 + _easycom_uni_badge2 + _easycom_uni_number_box2 + _easycom_uni_popup2)();
}
const _easycom_uni_icons = () => "../../../../../uni_modules/uni-icons/components/uni-icons/uni-icons.js";
const _easycom_uni_badge = () => "../../../../../uni_modules/uni-badge/components/uni-badge/uni-badge.js";
const _easycom_uni_number_box = () => "../../../../../uni_modules/uni-number-box/components/uni-number-box/uni-number-box.js";
const _easycom_uni_popup = () => "../../../../../uni_modules/uni-popup/components/uni-popup/uni-popup.js";
if (!Math) {
  (_easycom_uni_icons + _easycom_uni_badge + _easycom_uni_number_box + _easycom_uni_popup)();
}
const _sfc_main = {
  __name: "detail",
  setup(__props) {
    let userStore = store_modules_user.useUserStore();
    let goodsInfo = common_vendor.reactive({});
    let infoPopup = common_vendor.ref();
    let goodsId = common_vendor.ref();
    let allCount = common_vendor.ref(0);
    let blessing = common_vendor.ref("");
    let stepperValue = common_vendor.ref(1);
    let isStepper = common_vendor.ref(true);
    const previewImage = () => {
      common_vendor.index.previewImage({
        urls: goodsInfo.detailList
      });
    };
    const getGoodsInfo = async () => {
      let result = await api_goods.reqGoodsInfo(goodsId.value);
      Object.assign(goodsInfo, result.data);
    };
    const getCartCount = async () => {
      if (!userStore.token)
        return;
      let result = await api_cartReq.reqCartList();
      if (result.data.length != 0) {
        allCount.value = 0;
        result.data.forEach((item) => allCount.value += item.count);
      }
    };
    const addCart = () => {
      isStepper.value = true;
      infoPopup.value.open();
    };
    const buyGood = () => {
      isStepper.value = false;
      infoPopup.value.open();
    };
    const handlerSubmit = async () => {
      if (!userStore.token) {
        common_vendor.index.navigateTo({
          url: "/pages/login/login"
        });
        return;
      }
      if (isStepper.value) {
        if (!blessing.value.trim()) {
          utils_extendApi.toast({ title: "请输入祝福语" });
          return;
        }
        let result = await api_cartReq.reqAddCart({
          goodsId: goodsId.value,
          count: stepperValue.value,
          blessing: blessing.value
        });
        if (result.code == 200) {
          getCartCount();
          infoPopup.value.close();
          utils_extendApi.toast({ title: `此商品已加入 ${stepperValue.value} 个` });
        } else {
          utils_extendApi.toast({ title: `加入失败` });
        }
      } else {
        if (!blessing.value.trim()) {
          utils_extendApi.toast({ title: "请输入祝福语" });
          return;
        }
        common_vendor.index.navigateTo({
          url: `/modules/orderPayModule/pages/order/detail/detail?goodsId=${goodsId.value}&blessing=${blessing.value}`
        });
      }
    };
    common_vendor.onLoad((options) => {
      goodsId.value = options.goodsId;
      getGoodsInfo();
      getCartCount();
    });
    return (_ctx, _cache) => {
      return {
        a: common_vendor.unref(goodsInfo).imageUrl,
        b: common_vendor.o(previewImage),
        c: common_vendor.t(common_vendor.unref(goodsInfo).price),
        d: common_vendor.t(common_vendor.unref(goodsInfo).marketPrice),
        e: common_vendor.t(common_vendor.unref(goodsInfo).name),
        f: common_vendor.t(common_vendor.unref(goodsInfo).floralLanguage),
        g: common_vendor.f(common_vendor.unref(goodsInfo).detailList, (item, index, i0) => {
          return {
            a: index,
            b: item
          };
        }),
        h: common_vendor.p({
          type: "home",
          size: "25"
        }),
        i: common_vendor.p({
          type: "cart",
          size: "25"
        }),
        j: common_vendor.p({
          size: "small",
          text: common_vendor.unref(allCount),
          absolute: "rightTop"
        }),
        k: common_vendor.p({
          type: "chat",
          size: "25"
        }),
        l: common_vendor.o(addCart),
        m: common_vendor.o(buyGood),
        n: common_vendor.unref(goodsInfo).imageUrl,
        o: common_vendor.t(common_vendor.unref(goodsInfo).name),
        p: common_vendor.t(common_vendor.unref(goodsInfo).price),
        q: common_vendor.o(($event) => common_vendor.isRef(stepperValue) ? stepperValue.value = $event : stepperValue = $event),
        r: common_vendor.p({
          min: 1,
          max: 200,
          modelValue: common_vendor.unref(stepperValue)
        }),
        s: common_vendor.unref(isStepper),
        t: common_vendor.unref(blessing),
        v: common_vendor.o(($event) => common_vendor.isRef(blessing) ? blessing.value = $event.detail.value : blessing = $event.detail.value),
        w: common_vendor.o(handlerSubmit),
        x: common_vendor.sr(infoPopup, "c4bc6a47-4", {
          "k": "infoPopup"
        }),
        y: common_vendor.p({
          type: "bottom",
          ["safe-area"]: false
        })
      };
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-c4bc6a47"], ["__file", "E:/前端/微信小程序/代码/慕尚花坊/floor/modules/goodModule/pages/goods/detail/detail.vue"]]);
wx.createPage(MiniProgramPage);
