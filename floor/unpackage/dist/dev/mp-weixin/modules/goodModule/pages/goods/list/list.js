"use strict";
const common_vendor = require("../../../../../common/vendor.js");
const api_goods = require("../../../../../api/goods.js");
require("../../../../../utils/http.js");
require("../../../../../utils/request.js");
require("../../../../../utils/extendApi.js");
require("../../../../../utils/storage.js");
require("../../../../../utils/env.js");
if (!Array) {
  const _easycom_good_card2 = common_vendor.resolveComponent("good-card");
  const _easycom_not_logged_in2 = common_vendor.resolveComponent("not-logged-in");
  (_easycom_good_card2 + _easycom_not_logged_in2)();
}
const _easycom_good_card = () => "../../../../../components/good-card/good-card2.js";
const _easycom_not_logged_in = () => "../../../../../components/not-logged-in/not-logged-in.js";
if (!Math) {
  (_easycom_good_card + _easycom_not_logged_in)();
}
const _sfc_main = {
  __name: "list",
  setup(__props) {
    let requestData = common_vendor.reactive({
      page: 1,
      limit: 10,
      category1Id: "",
      category2Id: ""
    });
    let goodsList = common_vendor.ref([]);
    let isFinish = common_vendor.ref(false);
    let isLoading = common_vendor.ref(false);
    const getGoodsList = async () => {
      isLoading.value = true;
      let result = await api_goods.reqGoodsList(requestData);
      isLoading.value = false;
      goodsList.value = [...goodsList.value, ...result.data.records];
      if (result.data.records.length < requestData.limit) {
        isFinish.value = true;
      }
    };
    common_vendor.onReachBottom(() => {
      if (isLoading.value)
        return;
      if (isFinish.value)
        return;
      requestData.page++;
      getGoodsList();
    });
    common_vendor.onPullDownRefresh(() => {
      Object.assign(requestData, { ...requestData, page: 1 });
      goodsList.value = [];
      isFinish.value = false;
      isLoading.value = false;
      getGoodsList();
      common_vendor.index.stopPullDownRefresh();
    });
    common_vendor.onLoad((options) => {
      Object.assign(requestData, options);
      getGoodsList();
    });
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: common_vendor.unref(goodsList).length
      }, common_vendor.unref(goodsList).length ? {
        b: common_vendor.f(common_vendor.unref(goodsList), (item, index, i0) => {
          return {
            a: "042f6052-0-" + i0,
            b: common_vendor.p({
              goodItem: item
            }),
            c: index
          };
        }),
        c: common_vendor.unref(isFinish)
      } : {
        d: common_vendor.p({
          obj: {
            text: "该分类下暂无商品，去看看其他商品吧～",
            button: "查看其他商品",
            url: "/pages/category/category",
            toTabbar: true
          }
        })
      });
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-042f6052"], ["__file", "E:/前端/微信小程序/代码/慕尚花坊/floor/modules/goodModule/pages/goods/list/list.vue"]]);
wx.createPage(MiniProgramPage);
