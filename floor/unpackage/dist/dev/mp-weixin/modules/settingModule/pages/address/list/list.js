"use strict";
const common_vendor = require("../../../../../common/vendor.js");
const api_address = require("../../../../../api/address.js");
const utils_extendApi = require("../../../../../utils/extendApi.js");
require("../../../../../utils/http.js");
require("../../../../../utils/request.js");
require("../../../../../utils/storage.js");
require("../../../../../utils/env.js");
if (!Array) {
  const _easycom_uni_swipe_action_item2 = common_vendor.resolveComponent("uni-swipe-action-item");
  const _easycom_uni_swipe_action2 = common_vendor.resolveComponent("uni-swipe-action");
  const _easycom_not_logged_in2 = common_vendor.resolveComponent("not-logged-in");
  (_easycom_uni_swipe_action_item2 + _easycom_uni_swipe_action2 + _easycom_not_logged_in2)();
}
const _easycom_uni_swipe_action_item = () => "../../../../../uni_modules/uni-swipe-action/components/uni-swipe-action-item/uni-swipe-action-item.js";
const _easycom_uni_swipe_action = () => "../../../../../uni_modules/uni-swipe-action/components/uni-swipe-action/uni-swipe-action.js";
const _easycom_not_logged_in = () => "../../../../../components/not-logged-in/not-logged-in.js";
if (!Math) {
  (_easycom_uni_swipe_action_item + _easycom_uni_swipe_action + _easycom_not_logged_in)();
}
const _sfc_main = {
  __name: "list",
  setup(__props) {
    let addressList = common_vendor.ref([]);
    let options = common_vendor.ref(
      [{
        text: "删除",
        style: {
          backgroundColor: "#dd524d"
        }
      }]
    );
    let swipe = common_vendor.ref();
    let flag = common_vendor.ref(0);
    const closeAllSwipe = () => {
      swipe.value.closeAll();
    };
    const deleteItem = async (id) => {
      let modelRes = await common_vendor.index.showModal({
        title: "是否删除该地址"
      });
      if (modelRes.confirm) {
        await api_address.reqDelAddress(id);
        getAddressList();
        utils_extendApi.toast({ title: "删除成功" });
      }
    };
    const getAddressList = async () => {
      let result = await api_address.reqAddressList();
      if (result.code == 200) {
        let { data } = result;
        addressList.value = [...data];
      }
    };
    const toEdit = (id) => {
      common_vendor.index.navigateTo({
        url: `/modules/settingModule/pages/address/add/add?id=${id}`
      });
    };
    const changeAddress = (id) => {
      if (flag.value == 1) {
        let selectAdress = addressList.value.find((item) => item.id == id);
        getApp().globalData.address = selectAdress;
        common_vendor.index.navigateBack();
      }
    };
    common_vendor.onLoad((options2) => {
      flag.value = options2.flag;
    });
    common_vendor.onShow(async () => {
      getAddressList();
    });
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: common_vendor.unref(addressList).length
      }, common_vendor.unref(addressList).length ? {
        b: common_vendor.f(common_vendor.unref(addressList), (item, index, i0) => {
          return {
            a: common_vendor.t(item.name),
            b: common_vendor.t(item.phone.substr(0, 3) + "****" + item.phone.substr(-4, 4)),
            c: item.isDefault,
            d: common_vendor.t(item.fullAddress),
            e: common_vendor.o(($event) => changeAddress(item.id), item.id),
            f: common_vendor.o(($event) => toEdit(item.id), item.id),
            g: common_vendor.o(($event) => deleteItem(item.id), item.id),
            h: "1b364490-1-" + i0 + ",1b364490-0",
            i: item.id
          };
        }),
        c: common_vendor.p({
          ["right-options"]: common_vendor.unref(options)
        }),
        d: common_vendor.sr(swipe, "1b364490-0", {
          "k": "swipe"
        })
      } : {
        e: common_vendor.p({
          obj: {
            text: "还没有新增地址，快去添加吧~",
            button: ""
          }
        })
      }, {
        f: common_vendor.o(closeAllSwipe)
      });
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-1b364490"], ["__file", "E:/前端/微信小程序/代码/慕尚花坊/floor/modules/settingModule/pages/address/list/list.vue"]]);
wx.createPage(MiniProgramPage);
