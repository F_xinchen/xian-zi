"use strict";
const common_vendor = require("../../../../common/vendor.js");
const store_modules_user = require("../../../../store/modules/user.js");
const api_user = require("../../../../api/user.js");
const utils_storage = require("../../../../utils/storage.js");
const utils_extendApi = require("../../../../utils/extendApi.js");
require("../../../../utils/http.js");
require("../../../../utils/request.js");
require("../../../../utils/env.js");
const _sfc_main = {
  __name: "profile",
  setup(__props) {
    let userStore = store_modules_user.useUserStore();
    let userInfo = userStore.userInfo;
    const chooseAvatar = (e) => {
      let { avatarUrl } = e.detail;
      common_vendor.index.uploadFile({
        url: "https://gmall-prod.atguigu.cn/mall-api/fileUpload",
        filePath: avatarUrl,
        name: "file",
        header: {
          token: userStore.token
        },
        success: (res) => {
          let uploadRes = JSON.parse(res.data);
          userInfo.headimgurl = uploadRes.data;
        }
      });
    };
    const showPopup = () => {
      common_vendor.index.showModal({
        editable: true,
        title: "输入新昵称",
        content: ``,
        confirmColor: "#f3514f",
        success: (res) => {
          if (res.confirm) {
            if (!res.content.trim()) {
              utils_extendApi.toast({ title: "输入不能为空" });
              return;
            }
            userInfo.nickname = res.content;
          }
        }
      });
    };
    const updateUserInfo = async () => {
      let res = await api_user.reqUpdateUserInfo(userInfo);
      if (res.code == 200) {
        utils_storage.setStorage("userInfo", userInfo);
        userStore.setUserInfo(userInfo);
        utils_extendApi.toast({
          title: "用户信息更新成功"
        });
      }
    };
    return (_ctx, _cache) => {
      return {
        a: common_vendor.unref(userInfo).headimgurl,
        b: common_vendor.o(chooseAvatar),
        c: common_vendor.t(common_vendor.unref(userInfo).nickname),
        d: common_vendor.o(showPopup),
        e: common_vendor.o(updateUserInfo)
      };
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-57900cd5"], ["__file", "E:/前端/微信小程序/代码/慕尚花坊/floor/modules/settingModule/pages/profile/profile.vue"]]);
wx.createPage(MiniProgramPage);
