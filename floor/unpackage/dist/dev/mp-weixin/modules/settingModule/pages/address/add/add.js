"use strict";
const common_vendor = require("../../../../../common/vendor.js");
const utils_extendApi = require("../../../../../utils/extendApi.js");
const api_address = require("../../../../../api/address.js");
require("../../../../../utils/http.js");
require("../../../../../utils/request.js");
require("../../../../../utils/storage.js");
require("../../../../../utils/env.js");
if (!Array) {
  const _easycom_uni_forms_item2 = common_vendor.resolveComponent("uni-forms-item");
  const _easycom_uni_icons2 = common_vendor.resolveComponent("uni-icons");
  const _easycom_uni_forms2 = common_vendor.resolveComponent("uni-forms");
  (_easycom_uni_forms_item2 + _easycom_uni_icons2 + _easycom_uni_forms2)();
}
const _easycom_uni_forms_item = () => "../../../../../uni_modules/uni-forms/components/uni-forms-item/uni-forms-item.js";
const _easycom_uni_icons = () => "../../../../../uni_modules/uni-icons/components/uni-icons/uni-icons.js";
const _easycom_uni_forms = () => "../../../../../uni_modules/uni-forms/components/uni-forms/uni-forms.js";
if (!Math) {
  (_easycom_uni_forms_item + _easycom_uni_icons + _easycom_uni_forms)();
}
const _sfc_main = {
  __name: "add",
  setup(__props) {
    let addressInfo = common_vendor.reactive({
      name: "",
      // 收货人
      phone: "",
      // 手机号码
      provinceName: "",
      // 省
      provinceCode: "",
      // 省编码
      cityName: "",
      // 市
      cityCode: "",
      // 市编码
      districtName: "",
      // 区
      districtCode: "",
      // 市编码
      address: "",
      // 详细地址
      fullAddress: "",
      // 完整地址
      isDefault: false
      // 是否设置为默认地址，0 不设置为默认地址，1 设置为默认地址
    });
    let addressId = common_vendor.ref(null);
    let form = common_vendor.ref();
    const switchChange = (e) => {
      addressInfo.isDefault = e.detail.value;
    };
    const onAddressChange = (e) => {
      let addressArr = e.detail.value;
      let addressCode = e.detail.code;
      addressInfo.provinceName = addressArr[0];
      addressInfo.cityName = addressArr[1];
      addressInfo.districtName = addressArr[2];
      addressInfo.provinceCode = addressCode[0];
      addressInfo.cityCode = addressCode[1];
      addressInfo.districtCode = addressCode[2];
    };
    const onLocation = async () => {
      try {
        let { latitude, longitude, name } = await common_vendor.index.chooseLocation();
        common_vendor.index.request({
          url: `https://restapi.amap.com/v3/geocode/regeo?location=${longitude},${latitude}`,
          method: "GET",
          data: {
            output: "json",
            key: "9ac99f2ecf2b3cc3bd832705870682e3",
            radius: "1000",
            extensions: "all"
          },
          success: (res) => {
            let { adcode, province, city, district } = res.data.regeocode.addressComponent;
            let { street, number } = res.data.regeocode.addressComponent.streetNumber;
            let { formatted_address } = res.data.regeocode.formatted_address;
            addressInfo.provinceCode = adcode.replace(adcode.substring(2, 6), "0000");
            addressInfo.provinceName = province;
            addressInfo.cityCode = adcode.replace(adcode.substring(4, 6), "00");
            addressInfo.cityName = city;
            addressInfo.districtCode = district.length == 0 ? "" : adcode;
            addressInfo.districtName = district.length == 0 ? city : district;
            addressInfo.address = street + number + name;
            addressInfo.fullAddress = formatted_address + name;
          }
        });
      } catch (e) {
        utils_extendApi.toast({ title: "您拒绝授权获取位置信息" });
      }
    };
    const valiName = (rule, value, data, callback) => {
      let regexp = /^[\u4e00-\u9fa5]{2,6}$/;
      if (regexp.test(value)) {
        callback();
      } else {
        callback("请输入2-6个中文字符");
      }
    };
    const valiPhone = (rule, value, data, callback) => {
      let regexp = /^1[3-9]\d{9}$/;
      if (regexp.test(value)) {
        callback();
      } else {
        callback("请输入大陆手机号格式");
      }
    };
    let rules = common_vendor.ref({
      name: {
        rules: [
          {
            required: true,
            validateFunction: valiName,
            errorMessage: "请填写{label}"
          }
        ],
        label: "姓名"
      },
      phone: {
        rules: [
          {
            required: true,
            validateFunction: valiPhone,
            errorMessage: "请填写手机号"
          }
        ]
      },
      cityName: { rules: [{ required: true, errorMessage: "请填写省市区" }] },
      address: { rules: [{ required: true, errorMessage: "请填写详细地址" }] }
    });
    const submit = () => {
      addressInfo.fullAddress = addressInfo.provinceName + addressInfo.cityName + addressInfo.districtName + addressInfo.address;
      addressInfo.isDefault = addressInfo.isDefault ? 1 : 0;
      form.value.validate().then(async () => {
        if (addressId.value) {
          let resule = await api_address.reqUpdateAddress(addressInfo);
          if (resule.code == 200) {
            common_vendor.index.navigateBack();
            utils_extendApi.toast({ title: "更新成功" });
          }
        } else {
          let resule = await api_address.reqAddAddress(addressInfo);
          if (resule.code == 200) {
            common_vendor.index.navigateBack();
            utils_extendApi.toast({ title: "新增成功" });
          }
        }
      }).catch((err) => {
        utils_extendApi.toast({ title: "验证失败" });
      });
    };
    common_vendor.onLoad(async (options) => {
      let { id } = options;
      if (id) {
        addressId.value = id;
        let result = await api_address.reqAddressInfo(id);
        Object.assign(addressInfo, result.data);
      }
      common_vendor.index.setNavigationBarTitle({
        title: `${id ? "更新" : "新增"}收货地址`
      });
    });
    common_vendor.onReady(() => {
      form.value.setRules(rules.value);
    });
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: common_vendor.unref(addressInfo).name,
        b: common_vendor.o(($event) => common_vendor.unref(addressInfo).name = $event.detail.value),
        c: common_vendor.p({
          label: "收货人",
          name: "name"
        }),
        d: common_vendor.unref(addressInfo).phone,
        e: common_vendor.o(($event) => common_vendor.unref(addressInfo).phone = $event.detail.value),
        f: common_vendor.p({
          label: "手机号码",
          name: "phone"
        }),
        g: common_vendor.unref(addressInfo).provinceName
      }, common_vendor.unref(addressInfo).provinceName ? {
        h: common_vendor.t(common_vendor.unref(addressInfo).provinceName + " " + common_vendor.unref(addressInfo).cityName + " " + common_vendor.unref(addressInfo).districtName)
      } : {}, {
        i: [common_vendor.unref(addressInfo).provinceName, common_vendor.unref(addressInfo).cityName, common_vendor.unref(addressInfo).districtName],
        j: common_vendor.o(onAddressChange),
        k: common_vendor.p({
          type: "location",
          size: "20"
        }),
        l: common_vendor.o(onLocation),
        m: common_vendor.p({
          label: "所在地区",
          name: "cityName"
        }),
        n: common_vendor.unref(addressInfo).address,
        o: common_vendor.o(($event) => common_vendor.unref(addressInfo).address = $event.detail.value),
        p: common_vendor.p({
          label: "详细地址",
          name: "address"
        }),
        q: common_vendor.unref(addressInfo).isDefault,
        r: common_vendor.o(switchChange),
        s: common_vendor.p({
          label: "设置默认地址",
          name: "isDefault",
          ["label-width"]: 100
        }),
        t: common_vendor.sr(form, "0738fd27-0", {
          "k": "form"
        }),
        v: common_vendor.p({
          rules: common_vendor.unref(rules),
          ["model-value"]: common_vendor.unref(addressInfo)
        }),
        w: common_vendor.t(common_vendor.unref(addressId) ? "更新" : "新增"),
        x: common_vendor.o(submit)
      });
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-0738fd27"], ["__file", "E:/前端/微信小程序/代码/慕尚花坊/floor/modules/settingModule/pages/address/add/add.vue"]]);
wx.createPage(MiniProgramPage);
