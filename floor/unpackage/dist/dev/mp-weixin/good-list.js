"use strict";
const common_vendor = require("./common/vendor.js");
if (!Array) {
  const _easycom_good_card2 = common_vendor.resolveComponent("good-card");
  _easycom_good_card2();
}
const _easycom_good_card = () => "./components/good-card/good-card2.js";
if (!Math) {
  _easycom_good_card();
}
const _sfc_main = {
  __name: "good-list",
  props: ["title", "list"],
  setup(__props) {
    return (_ctx, _cache) => {
      return {
        a: common_vendor.t(__props.title),
        b: common_vendor.f(__props.list, (item, index, i0) => {
          return {
            a: "9d63bf6f-0-" + i0,
            b: common_vendor.p({
              goodItem: item
            }),
            c: index
          };
        })
      };
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-9d63bf6f"], ["__file", "E:/前端/微信小程序/代码/慕尚花坊/floor/components/good-list/good-list.vue"]]);
exports.MiniProgramPage = MiniProgramPage;
