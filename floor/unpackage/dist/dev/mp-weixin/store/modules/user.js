"use strict";
const common_vendor = require("../../common/vendor.js");
const utils_storage = require("../../utils/storage.js");
const useUserStore = common_vendor.defineStore("User", {
  state: () => {
    return {
      token: utils_storage.getStorage("token") || "",
      userInfo: utils_storage.getStorage("userInfo") || {}
    };
  },
  actions: {
    setToken(token) {
      this.token = token;
    },
    setUserInfo(userInfo) {
      this.userInfo = userInfo;
    }
  },
  getters: {}
});
exports.useUserStore = useUserStore;
