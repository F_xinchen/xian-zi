"use strict";
const common_vendor = require("./common/vendor.js");
const common_assets = require("./common/assets.js");
const _sfc_main = {
  __name: "good-card",
  props: ["goodItem"],
  setup(__props) {
    const _goodItem = __props;
    return (_ctx, _cache) => {
      return {
        a: `${common_vendor.unref(_goodItem).goodItem.imageUrl}`,
        b: common_vendor.t(common_vendor.unref(_goodItem).goodItem.name),
        c: common_vendor.t(common_vendor.unref(_goodItem).goodItem.floralLanguage),
        d: common_vendor.t(common_vendor.unref(_goodItem).goodItem.price),
        e: common_vendor.t(common_vendor.unref(_goodItem).goodItem.marketPrice),
        f: common_assets._imports_0,
        g: `/modules/goodModule/pages/goods/detail/detail?goodsId=${common_vendor.unref(_goodItem).goodItem.id}`
      };
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-5b187298"], ["__file", "E:/前端/微信小程序/代码/慕尚花坊/floor/components/good-card/good-card.vue"]]);
exports.MiniProgramPage = MiniProgramPage;
