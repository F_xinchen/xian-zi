"use strict";
const common_vendor = require("../common/vendor.js");
class Request {
  // 用于创建初始化类的属性和方法
  constructor(params = {}) {
    this.defaults = {
      baseURL: "",
      method: "GET",
      data: null,
      url: "",
      header: {
        "Content-type": "application/json"
        //数据交互格式
      },
      timeout: 3e3,
      isLoading: true
      // 是否使用默认loading效果
    };
    // 拦截器对象
    this.interceptors = {
      request: (config) => {
        return config;
      },
      response: (response) => {
        return response;
      }
    };
    // 数组队列，初始值为空数组，用于储存请求队列，请求标识
    this.queue = [];
    this.defaults = Object.assign({}, this.defaults, params);
  }
  request(options) {
    this.timerId && clearTimeout(this.timerId);
    options.url = this.defaults.baseURL + options.url;
    options = Object.assign({}, this.defaults, options);
    if (options.isLoading && options.method !== "UPLOAD") {
      this.queue.length == 0 && common_vendor.index.showLoading({});
      this.queue.push("request");
    }
    options = this.interceptors.request(options);
    return new Promise((resolve, reject) => {
      if (options.method === "UPLOAD") {
        common_vendor.index.uploadFile({
          ...options,
          success: (res) => {
            res.data = JSON.parse(res.data);
            const mergeRes = Object.assign({}, res, {
              config: options,
              isSuccess: true
            });
            resolve(this.interceptors.response(mergeRes));
          },
          fail: (err) => {
            const mergeErr = Object.assign({}, err, {
              config: options,
              isSuccess: false
            });
            reject(this.interceptors.response(mergeErr));
          }
        });
      } else {
        common_vendor.index.request({
          ...options,
          success: (res) => {
            let mergeRres = Object.assign({}, res, {
              config: options,
              isSuccess: true
            });
            resolve(this.interceptors.response(mergeRres));
          },
          fail: (err) => {
            let mergeErr = Object.assign({}, err, {
              config: options,
              isSuccess: false
            });
            reject(this.interceptors.response(mergeErr));
          },
          complete: () => {
            if (options.isLoading) {
              this.queue.pop();
              this.queue.length == 0 && this.queue.push("request");
              this.timerId = setTimeout(() => {
                this.queue.pop();
                this.queue.length == 0 && common_vendor.index.hideLoading();
                clearTimeout(this.timerId);
              }, 100);
            }
          }
        });
      }
    });
  }
  get(url, data = {}, config = {}) {
    return this.request(Object.assign({ url, data, method: "GET" }, config));
  }
  post(url, data = {}, config = {}) {
    return this.request(Object.assign({ url, data, method: "POST" }, config));
  }
  delete(url, data = {}, config = {}) {
    return this.request(Object.assign({ url, data, method: "DELETE" }, config));
  }
  put(url, data = {}, config = {}) {
    return this.request(Object.assign({ url, data, method: "PUT" }, config));
  }
  all(...params) {
    return Promise.all(params);
  }
  upload(url, filePath, name = "file", config = {}) {
    return this.request(Object.assign({ url, filePath, name, method: "UPLOAD" }, config));
  }
}
exports.Request = Request;
