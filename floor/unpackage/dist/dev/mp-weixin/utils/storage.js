"use strict";
const common_vendor = require("../common/vendor.js");
const setStorage = (key, value) => {
  try {
    common_vendor.index.setStorageSync(key, value);
  } catch (e) {
    console.error(`本地储存 ${key},发生了异常 =>`, e);
  }
};
const getStorage = (key) => {
  try {
    return common_vendor.index.getStorageSync(key);
  } catch (e) {
    console.error(`本地获取 ${key},发生了异常 =>`, e);
  }
};
const clearStorage = (key) => {
  try {
    common_vendor.index.clearStorageSync();
  } catch (e) {
    console.error(`本地清除,发生了异常 =>`, e);
  }
};
exports.clearStorage = clearStorage;
exports.getStorage = getStorage;
exports.setStorage = setStorage;
