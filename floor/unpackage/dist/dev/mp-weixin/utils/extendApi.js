"use strict";
const common_vendor = require("../common/vendor.js");
const toast = ({ title = "加载中...", icon = "none", duration = 2e3, mask = true } = {}) => {
  common_vendor.index.showToast({
    title,
    icon,
    duration,
    mask
  });
};
const modal = (option = {}) => {
  return new Promise((resolve) => {
    const defaultOpt = {
      title: "提示",
      content: "您确定执行该操作吗？",
      confirmColor: "#f3514f"
    };
    let opts = Object.assign({}, defaultOpt, option);
    common_vendor.index.showModal({
      ...opts,
      success: ({ cancel, confirm }) => {
        if (confirm) {
          resolve(true);
        } else {
          resolve(false);
        }
      }
    });
  });
};
exports.modal = modal;
exports.toast = toast;
