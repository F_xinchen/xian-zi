"use strict";
const common_vendor = require("../common/vendor.js");
const utils_request = require("./request.js");
const utils_extendApi = require("./extendApi.js");
const utils_storage = require("./storage.js");
const utils_env = require("./env.js");
const instance = new utils_request.Request({
  baseURL: utils_env.env.baseURL,
  timeout: 6e3
});
instance.interceptors.request = (config) => {
  let token = utils_storage.getStorage("token");
  if (token) {
    config.header["token"] = token;
  }
  return config;
};
instance.interceptors.response = async (response) => {
  let { isSuccess, data } = response;
  if (!isSuccess) {
    common_vendor.index.showToast({
      title: "网络异常请重试",
      icon: "none"
    });
    return response;
  }
  switch (data.code) {
    case 200:
      return data;
    case 208:
      const res = await utils_extendApi.modal({
        content: "鉴权失败,请重新登录",
        showCancel: false
      });
      if (res) {
        utils_storage.clearStorage();
        common_vendor.index.navigateTo({
          url: "/pages/login/login"
        });
      }
      return response;
    default:
      utils_extendApi.toast({
        title: "程序出现异常，请联系客服或稍后重试"
      });
      return Promise.reject(response);
  }
};
exports.instance = instance;
