"use strict";
const common_vendor = require("../common/vendor.js");
const { miniProgram } = common_vendor.index.getAccountInfoSync();
const { envVersion } = miniProgram;
let env = {
  baseURL: "https://gmall-prod.atguigu.cn/mall-api"
};
switch (envVersion) {
  case "develop":
    env.baseURL = "https://gmall-prod.atguigu.cn/mall-api";
    break;
  case "trial":
    env.baseURL = "https://gmall-prod.atguigu.cn/mall-api";
    break;
  case "release":
    env.baseURL = "https://gmall-prod.atguigu.cn/mall-api";
    break;
}
exports.env = env;
