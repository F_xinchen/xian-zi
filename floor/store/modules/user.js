import { defineStore } from 'pinia'
import { getStorage } from "@/utils/storage.js"

const useUserStore = defineStore('User', {
	state: () => {
		return {
			token: getStorage('token') || '',
			userInfo: getStorage('userInfo') || {}
		}
	},
	actions: {
		setToken(token) {
			this.token = token
		},
		setUserInfo(userInfo) {
			this.userInfo = userInfo
		}
	},
	getters: {

	}
})

export default useUserStore